import {Normalizer} from "@interfaces/normalize";
import {RawEntry} from "@interfaces/raw-entry";
import {isEmptyString, isNonNullAndUndefined} from "../utils/object-utils";

export class TransformDefaultValue implements Normalizer {
  private _property: string;
  private _defaultValue: any;

  constructor(property: string, defaultValue: any) {
    this._property = property;
    this._defaultValue = defaultValue;

  }

  transform(entry: RawEntry): RawEntry {
    if (isEmptyString(entry[this._property])) {
      entry[this._property] = this._defaultValue;
      return {
        ...entry,
        [this._property]: this._defaultValue
      };
    }

    return { ...entry };
  }

  get name(): string { return `Default value ${this._property}=${this._defaultValue}`};
}
