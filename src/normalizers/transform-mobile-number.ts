import {Normalizer} from "@interfaces/normalize";
import {RawEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";

export class TransformMobileNumber implements Normalizer {
  transform(entry: RawEntry): RawEntry {
    if (!isNonNullAndUndefined(entry.mobileNumber)) return { ... entry };

    const length = entry.mobileNumber.length;

    if (length === 12) {
      // 639171234567 to 09xxx
      entry.mobileNumber = transform639xxto09xx(entry.mobileNumber);
    } else if (length === 13) {
      // +639xx to 09xx
      entry.mobileNumber = transformPlus639xxto09xx(entry.mobileNumber);
    } else if (length === 10) {
      // 9xx to 09xx
      entry.mobileNumber = transform9xxto09xx(entry.mobileNumber);
    }

    return { ... entry};
  }

  get name(): string { return `Transform mobile number` };


}

/**
 * Excel removes a 0 if it considers it a number.
 * @param mobileNumber
 */
export function transform9xxto09xx(mobileNumber: string): string {
  if (!isNonNullAndUndefined(mobileNumber)) return null;

  if (mobileNumber.startsWith("9") && mobileNumber.length === 10)
    mobileNumber =  "0" + mobileNumber;

  return mobileNumber;
}

/**
 * Normalization to 09xx.
 * @param mobileNumber
 */
export function transform639xxto09xx(mobileNumber: string): string {
  if (!isNonNullAndUndefined(mobileNumber)) return null;

  if (mobileNumber.startsWith("63") && length === 12)
    mobileNumber =  "0" + mobileNumber.slice(2);

  return mobileNumber;
}

/**
 * Normalization to 09xx.
 * @param mobileNumber
 */
export function transformPlus639xxto09xx(mobileNumber: string): string {
  if (!isNonNullAndUndefined(mobileNumber)) return null;

  if (mobileNumber.startsWith("+63") && length === 13)
    mobileNumber =  "0" + mobileNumber.slice(3);

  return mobileNumber;
}
