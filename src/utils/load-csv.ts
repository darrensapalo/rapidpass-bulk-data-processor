
import csv from 'csv-parser';
import fs from 'fs';
import {Observable} from "rxjs";
import {RawEntry} from "@interfaces/raw-entry";
import {headers} from "../constants/csv-headers";

export function loadFile(filePath: string): Observable<RawEntry> {
    return new Observable(subscriber => {
        fs.createReadStream(filePath)
          .pipe(csv({
            headers: headers,
            skipLines: 1,
          }))
          .on('data', (data) => {
            subscriber.next(data);
          })
          .on('end', () => {
              subscriber.complete();
          })
          .on('error', (err) => {
              subscriber.error(err);
        });
    })
}
