
export function isNonNullAndUndefined(object: any|null|undefined): object is any {
  return object !== null && object !== undefined;
}

export function isEmptyString(value: string|null|undefined): boolean {

  if (!isNonNullAndUndefined(value)) return true;

  return value === "";
}
