import {Observable} from "rxjs";
import { execFile } from 'child_process';
import fs from 'fs';

export function loadFiles(): Observable<string> {
  return new Observable<string>(subscriber => {

    fs.readdir('input', ((err, files) => {

      if (err) {
        subscriber.error(err);
        return;
      }

      files.map(file => `input/${file}`)
        .forEach(filePath => subscriber.next(filePath));

      subscriber.complete();

    }))
  });
}
