
export interface RawEntry {
  passType: string;
  aporType: string;

  firstName: string;
  middleName: string;
  lastName: string;

  suffix: string;
  company: string;
  idType: string;
  identifierNumber: string;

  plateNumber: string;
  mobileNumber: string;

  email: string;

  originName: string;
  originStreet: string;
  originCity: string;
  originProvince: string;

  destName: string;
  destStreet: string;
  destCity: string;
  destProvince: string;
  remarks: string;
}

export interface ValidatedEntry extends RawEntry {

  meta: {
    // Plate number for vehicles, mobile number for individuals
    referenceId?: string;
    passType?: 'INDIVIDUAL'|'VEHICLE';
    name?: string;
    isValid?: boolean;
    errors?: string[];
  }
}

export interface ResultEntry {

}
