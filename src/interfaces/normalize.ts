import {RawEntry} from "@interfaces/raw-entry";

export interface Normalizer {
  name: string;
  /**
   * @return the transformed object.
   * @param entry
   */
  transform(entry: RawEntry): RawEntry;
}
