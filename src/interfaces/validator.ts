import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {Observable} from "rxjs";

export interface ValidationRule {
  /**
   * @return a string describing the error, or `undefined` if it is valid.
   * @param entry
   */
  validate(entry: ValidatedEntry): Observable<ValidatedEntry>;

  validateNonRx(entry: ValidatedEntry): ValidatedEntry;
}
