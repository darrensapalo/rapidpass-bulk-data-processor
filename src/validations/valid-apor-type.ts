import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {from, Observable, of} from "rxjs";
import {PostgreSQLManager} from "../database/PostgreSQLManager";
import {map, mapTo, mergeMap, tap, toArray} from "rxjs/operators";

export class ValidAporType implements ValidationRule {

  private aporValues: string[];

  constructor(private psql: PostgreSQLManager) {

  }

  load() {
    return this.psql.query("SELECT * FROM lookup_table WHERE key IN ('APOR')")
      .pipe(
        map(row => row.rows),
        mergeMap(entry => from(entry.values())),
        map((value: {value: string}) => value.value),
        toArray(),
        tap((aporValues: string[]) => {
          this.aporValues = aporValues;
        })
      );
  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {
    if (!this.aporValues.includes(entry.aporType)) {

      return {
        ...entry,
        meta: {
          ...entry.meta,
          errors: [...entry.meta.errors, 'Invalid Apor Type (' + entry.aporType + ').']
        }
      };
    }

    return {
      ...entry,
      meta: {
        ...entry.meta,
        errors: [...entry.meta.errors]
      }
    };


  }

  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
