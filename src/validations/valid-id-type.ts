import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {from, Observable, of} from "rxjs";
import {PostgreSQLManager} from "../database/PostgreSQLManager";
import {map, mapTo, mergeMap, tap, toArray} from "rxjs/operators";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {log} from "../utils/logger";

export class ValidIdType implements ValidationRule {
  private idTypes: string[];
  constructor(private psql: PostgreSQLManager) {

  }

  load() {
    const sql = "SELECT * FROM lookup_table WHERE key IN ('IDTYPE-I', 'IDTYPE-V')";

    return this.psql.query(sql)
      .pipe(
        map(row => row.rows),
        mergeMap(entry => from(entry.values())),
        map((value: {value: string}) => value.value),
        toArray(),
        tap((idTypes: string[]) => {
          this.idTypes = idTypes;
        })
      );
  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    if (!this.idTypes.includes(entry.idType)) {


      return {
        ...entry,
        meta: {
          ...entry.meta,
          errors: [...entry.meta.errors, 'Invalid ID Type (idType=' + entry.idType + ').']
        }
      };
    }



    return {
      ...entry,
      meta: {
        ...entry.meta,
        errors: [...entry.meta.errors]
      }
    };

  }

  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }

}
