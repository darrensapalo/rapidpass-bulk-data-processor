import {ValidationRule} from "../interfaces/validator";
import {RawEntry, ValidatedEntry} from "../interfaces/raw-entry";
import {Observable, of} from "rxjs";
import {PostgreSQLManager} from "../database/PostgreSQLManager";
import {isEmptyString} from "../utils/object-utils";

export class ValidVehicleHasPlateNumber implements ValidationRule {

  constructor() {

  }

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {
    if (entry.meta.passType === "VEHICLE" && isEmptyString(entry.plateNumber)) {
      const errorMessage = 'Plate number is required for vehicle passes.';
      return {
        ...entry,
        meta: {
          ...entry.meta,
          errors: [...entry.meta.errors, errorMessage]
        }
      };
    }

    return {
      ...entry,
      meta: {
        ...entry.meta,
        errors: [...entry.meta.errors]
      }
    };;
  }

  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
