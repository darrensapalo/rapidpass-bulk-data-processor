import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {Observable, of} from "rxjs";

export class ValidMobileNumberPattern implements ValidationRule {

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {
    if (isNonNullAndUndefined(entry)) {
      const mobileNumber = entry.mobileNumber;

      const regex = RegExp("^09\\d{9}$");

      if (regex.test(mobileNumber)) {
        return {
          ...entry,
          meta: {
            ...entry.meta,
            errors: [...entry.meta.errors]
          }
        };
      }
    }

    return {
      ...entry,
      meta: {
        ...entry.meta,
        errors: [...entry.meta.errors, "Invalid mobile number pattern. It should start with 09XX and should be 11 characters."]
      }
    };
  }

  validate(entry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
