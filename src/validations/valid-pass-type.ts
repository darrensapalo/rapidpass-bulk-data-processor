import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {Observable, of} from "rxjs";

export class ValidPassType implements ValidationRule {

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    if (!["INDIVIDUAL", "VEHICLE"].includes(entry.passType)) {
      const errorMessage = 'Invalid ID Type (passType=' + entry.passType + ').';
      return {
        ...entry,
        meta: {
          ...entry.meta,
          errors: [...entry.meta.errors, errorMessage]
        }
      };
    }

    return {
      ...entry,
      meta: {
        ...entry.meta,
        errors: [...entry.meta.errors]
      }
    };
  }


  validate(validatedEntry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }
}
