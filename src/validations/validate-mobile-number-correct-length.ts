import {ValidationRule} from "@interfaces/validator";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {isNonNullAndUndefined} from "../utils/object-utils";
import {Observable, of} from "rxjs";

export class ValidateMobileNumberCorrectLength implements ValidationRule {

  validateNonRx(entry: ValidatedEntry): ValidatedEntry {

    const mobileNumberExists = isNonNullAndUndefined(entry.mobileNumber);

    const length = mobileNumberExists ? entry.mobileNumber.length : 0;

    const exactLengthMatch = length === 11;

    // No errors.
    if (mobileNumberExists && exactLengthMatch) return {
      ...entry,
      meta: {
        ...entry.meta
      }
    };

    const errorMessage = 'Invalid mobile number length (len=' + length + '); expecting 09XXXXXXXXX format.';

    return {
      ...entry,
      meta: {
        ...entry.meta,
        errors: [...entry.meta.errors, errorMessage]
      }
    };
  }

  validate(entry: ValidatedEntry): Observable<ValidatedEntry> {
    return null;
  }

}
