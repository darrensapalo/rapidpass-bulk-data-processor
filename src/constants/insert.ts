import {ValidatedEntry} from "@interfaces/raw-entry";

const PARAMS = [
  'reference_id',
  'pass_type',
  'apor_type',
  'control_code',
  'id_type',
  'identifier_number',
  'name',
  'company',
  'plate_number',
  'remarks',
  'scope',
  'limitations',
  'origin_name',
  'origin_street',
  'origin_province',
  'origin_city',
  'destination_name',
  'destination_street',
  'destination_city',
  'destination_province',
  'valid_from',
  'valid_to',
  'issued_by',
  'updates',
  'status',
  'source',
  'date_time_created',
  'date_time_updated',
  'last_used_on',
  'registrant_id'
];

const VALUES = PARAMS.map((val, index) => `$${index + 1}`).join(", ");

/**
 * Follows the format:
 *
 * ```
 * INSERT INTO access_passes (params, ...) VALUES ($1, $2...)
 * ```
 */
export const INSERT_STATEMENT = `INSERT INTO access_pass (${PARAMS}) VALUES (${VALUES})`;

/**
 * Returns the validated entries into the correct order for the insert statement.
 * @param entry
 */
export function transformEntryToParameters(entry: ValidatedEntry): any[] {

  const referenceId = (entry.meta.passType === "INDIVIDUAL") ? entry.mobileNumber : entry.plateNumber;

  const now = new Date();

  return [
    referenceId,
    entry.passType,
    entry.aporType,
    null,
    entry.idType,
    entry.identifierNumber,
    entry.meta.name,
    entry.company,
    entry.plateNumber,
    entry.remarks,
    null,
    null,
    entry.originName,
    entry.originStreet,
    entry.originProvince,
    entry.originCity,
    entry.destName,
    entry.destStreet,
    entry.destCity,
    entry.destProvince,
    now,
    now,
    undefined,
    undefined,
    'PENDING',
    'BULK',
    now,
    now,
    undefined,
    1
  ];
}
