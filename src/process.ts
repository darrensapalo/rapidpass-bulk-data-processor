import {log} from "./utils/logger";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {Result} from "@interfaces/result";
import {QueryResult} from "pg";

export const processBulkData = (queryResult: any) => {
  // log.info("Successfully inserted " + validatedEntry.meta.referenceId + ".");
};

export function displayError(invalidatedEntry: ValidatedEntry){

  console.log("fail");

  const error = {
    mobileNumber: invalidatedEntry.mobileNumber,
    plateNumber: invalidatedEntry.plateNumber,
    errors: invalidatedEntry.meta.errors
  };

  log.error("Failed to insert " + invalidatedEntry.meta.referenceId + ".", error);

}

export function operationFailed(error: any) {
  log.error("Failed to execute operation.", error);
}
