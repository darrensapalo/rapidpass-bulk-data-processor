import dotenv from 'dotenv';
import {loadFile} from "./utils/load-csv";
import {
  bufferTime,
  concatMap,
  mergeMap, mergeMapTo, observeOn,
  publish, share, take, zipAll
} from "rxjs/operators";
import {filterByValid} from "./processing/filter";
import {normalize} from "./processing/normalize";
import {validate} from "./processing/validate";
import {
  asapScheduler,
  asyncScheduler,
  ConnectableObservable, from,
  Observable,
  zip
} from "rxjs";
import {ValidatedEntry} from "@interfaces/raw-entry";

import {ValidateRequiredField} from "./validations/validate-required-field";
import {psql} from "./database/PostgreSQLManager";
import {displayError, operationFailed, processBulkData} from "./process";
import {TransformMobileNumber} from "./normalizers/transform-mobile-number";
import {ValidateMobileNumberCorrectLength} from "./validations/validate-mobile-number-correct-length";
import {ValidPassType} from "./validations/valid-pass-type";
import {ValidAporType} from "./validations/valid-apor-type";
import {ValidIdType} from "./validations/valid-id-type";
import {ValidVehicleHasPlateNumber} from "./validations/valid-vehicle-has-plate-number";
import {ValidMobileNumberPattern} from "./validations/valid-mobile-number-pattern";
import {TransformDefaultValue} from "./normalizers/transform-default-value";
import {TransformPopulateMetadata} from "./normalizers/transform-populate-metadata";
import {log} from "./utils/logger";
import { loadFiles} from "./utils/load-input-files";
import {insertData} from "./processing/insert";

dotenv.config();

const startTime = new Date().getTime();

const normalizers = [
  new TransformMobileNumber(),
  new TransformDefaultValue("email", "no-reply@devcon.ph"),
  new TransformDefaultValue("remarks", "frontliner"),
  new TransformDefaultValue("identifierNumber", "OTH"),
  new TransformPopulateMetadata()
];

const AporTypeValidator = new ValidAporType(psql);

const IdTypeValidator = new ValidIdType(psql);


const validators = [
  new ValidateMobileNumberCorrectLength(),
  new ValidMobileNumberPattern(),
  new ValidateRequiredField("firstName"),
  new ValidateRequiredField("lastName"),
  new ValidateRequiredField("company"),
  new ValidateRequiredField("idType"),
  new ValidateRequiredField("mobileNumber"),
  new ValidPassType(),
  AporTypeValidator,
  IdTypeValidator,
  new ValidVehicleHasPlateNumber(),
  // new ValidHasNoExistingApprovedOrPendingAccessPass(psql)
];

const initializeAndLoadFiles$ = zip(AporTypeValidator.load(), IdTypeValidator.load()).pipe(
  mergeMapTo(loadFiles())
);

const validatedEntries$: Observable<ValidatedEntry>  =
  initializeAndLoadFiles$
  .pipe(
    mergeMap(filePath => loadFile(filePath), 15),
    observeOn(asapScheduler),
    normalize(normalizers),
    validate(validators),
    share()
  );

// Performs the insert operations on the database.
validatedEntries$.pipe(
  filterByValid(true),
  insertData()
).subscribe({
  next: processBulkData,
  error: operationFailed,
  complete: () => {
    console.log("completed!");

    const endTime = new Date().getTime();

    const ms = endTime - startTime;
    const totalSeconds = ms / 1000;

    console.log("start " + startTime);

    console.log("end " + endTime);

    log.info(`Successfully finished all processing in ${totalSeconds} seconds.`);
  }
});

// Collects the erroneous entries.
validatedEntries$.pipe(
  filterByValid(false)
).subscribe({
  next: displayError,
  error: operationFailed,
  complete: () => {
    console.log("failed finished")
  }
});
