import {concatMap, map, mapTo, mergeMap, tap} from "rxjs/operators";
import {ValidatedEntry} from "@interfaces/raw-entry";
import {ValidationRule} from "@interfaces/validator";
import {from, Observable, of} from "rxjs";

export function validate(validationRules: ValidationRule[]){

  return map<ValidatedEntry, ValidatedEntry>((value: ValidatedEntry) => {

    for (const vr of validationRules) {
      vr.validateNonRx(value);
    }

    value.meta.isValid = value.meta.errors.length === 0;

    return value;

  });
}
