import {concatMap, mergeMap, subscribeOn, tap} from "rxjs/operators";
import {ResultEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {asyncScheduler, Observable} from "rxjs";
import {psql} from "../database/PostgreSQLManager";
import {
  INSERT_STATEMENT,
  transformEntryToParameters
} from "../constants/insert";
import {QueryResult} from "pg";

export function insertData() {
  return mergeMap<ValidatedEntry, Observable<QueryResult<any>>>((entry, i) => {
    if (i % 1000 === 0) {
      console.log("Inserting for " + i);
    }
    const params = transformEntryToParameters(entry);

    return psql.query(INSERT_STATEMENT, params)
      .pipe(subscribeOn(asyncScheduler));
  });
}
