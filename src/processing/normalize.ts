import {map} from "rxjs/operators";
import {RawEntry, ValidatedEntry} from "@interfaces/raw-entry";
import {Normalizer} from "@interfaces/normalize";

export function normalize(transformers: Normalizer[]){
  return  map<RawEntry, ValidatedEntry>((value: RawEntry) => {

    let entry = transformers.reduce((acc, transformer) => transformer.transform(acc), value) as ValidatedEntry;

    return { ... entry, meta: {
      ...entry.meta
      } } as ValidatedEntry;
  });
}
