export function selectFile(): string {

  if (process.argv.length >= 3) return process.argv[2];

  if (process.env.TARGET_FILE !== null && process.env.TARGET_FILE !== undefined) return process.env.TARGET_FILE;

  return 'test.csv';
}
