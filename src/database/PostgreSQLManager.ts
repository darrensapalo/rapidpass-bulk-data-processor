import Pool from "pg-pool";
import {Client, PoolClient, QueryResult} from "pg";
import {defer, Observable} from "rxjs";
import dotenv from 'dotenv';
import {mergeMap} from "rxjs/operators";
dotenv.config();

export class PostgreSQLManager {
  private _pool: Pool<Client>;

  get pool(): Pool<Client> {
    return this._pool;
  }

  public constructor() {

    const psqlConfig = {
      database: process.env.POSTGRESQL_DB,
      user: process.env.POSTGRESQL_USER,
      password: process.env.POSTGRESQL_PASS,
      port: Number(process.env.POSTGRESQL_PORT),
      max: Number(process.env.POSTGRESQL_MAX_POOL),
      idleTimeoutMillis: Number(process.env.POSTGRESQL_CONN_TIMEOUT)
    };

    this._pool = new Pool(psqlConfig);
  }

  connect() {
    return defer(() => this._pool.connect());
  }

  query(queryStatement: string, parameters?: any[]): Observable<QueryResult<any>> {
    return this.connect()
      .pipe(
        mergeMap((client: PoolClient) => this.runQuery(client, queryStatement, parameters))
      )
  }

  runQuery(client: PoolClient, query: string, parameters: any[]) {
    return new Observable<QueryResult<any>>(subscriber => {
      client.query(query, parameters).then(value => {
      client.release(true);
      subscriber.next(value);
      subscriber.complete();
      })
      .catch((error) => subscriber.error(error));
    });
  }


}


function exitHandler(options, exitCode) {
  if (options.cleanup) console.log('clean');
  if (exitCode || exitCode === 0) console.log(exitCode);

  psql.pool.end().then(() => console.log("pool has been cleaned."));

  if (options.exit) process.exit();
}

process.on('exit', exitHandler.bind(null,{cleanup:true}));


export const psql = new PostgreSQLManager();
